#!/usr/bin/env bash
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

function new_image () {
    DISK="$1"
    if [ ! -f ${DISK} ]; then
        echo "Creating disk image ${DISK}..."
        dd if=/dev/zero of=${DISK} bs=1M count=50
        LOOPDEV=`losetup -f`
        if [ ! -z ${LOOPDEV} ]; then
            echo "Binding loopback device ${LOOPDEV} to disk..."
            losetup ${LOOPDEV} ${DISK}
            echo "Format ${LOOPDEV}..."
            mkfs.btrfs ${LOOPDEV}
            echo "Detaching loopback device..."
            losetup -d ${LOOPDEV}
        else
            echo "Cannot create loopback device. Aborting."
            exit 1
        fi
    else
        echo "Disk image ${DISK} already exists..."
    fi
}

function create_images () {
    DISKNUM=$1
    if [ ${DISKNUM} -gt 0 ]; then
        # Create loopback devices
        for i in `seq ${DISKNUM}`; do
            DISK=disk${i}.img
            new_image ${DISK}
        done
    fi
}

function remove_images () {
    DISKNUM=$1
    if [ ${DISKNUM} -gt 0 ]; then
        # Create loopback devices
        for i in `seq ${DISKNUM} -1 1`; do
            DISK=disk${i}.img
            if [ -f ${DISK} ]; then
                echo "Removing disk image ${DISK}..."
                rm -f ${DISK}
            fi
        done
    fi
}

function mount_images () {
    # Mount loopback devices
    DISKNUM=$1
    if [ ${DISKNUM} -gt 0 ]; then
        # Create loopback devices
        for i in `seq ${DISKNUM}`; do
            DISK=disk${i}.img
            LOOPDEV=`losetup -j ${DISK} | cut -d: -f1`
            if [ ! -z "${LOOPDEV}" ]; then
                echo "${DISK} already bounded to ${LOOPDEV}..."
            else
                LOOPDEV=`losetup -f`
                if [ -f ${DISK} ] & [ ! -z ${LOOPDEV} ]; then
                    echo "Binding loopback device ${LOOPDEV} to ${DISK}..."
                    losetup ${LOOPDEV} ${DISK}
                fi
            fi
            # check mount path
            MNTPATH=disk${i}
            if [ ! -d ${MNTPATH} ]; then
                echo "Creating mount path ${MNTPATH}..."
                mkdir -p ${MNTPATH}
            fi
            # mount device
            if ! mount | grep -q ${LOOPDEV}; then
                echo "Mounting ${LOOPDEV} to ${MNTPATH}..."
                sudo mount -t auto ${LOOPDEV} ${MNTPATH}
                sudo chown ${USER}:${USER} ${MNTPATH}
            else
                echo "${MNTPATH} is already mounted..."
            fi
        done
    fi
}

function umount_images () {
    # Unmount loopback devices
    DISKNUM=$1
    if [ ${DISKNUM} -gt 0 ]; then
        for i in `seq ${DISKNUM} -1 1`; do
            DISK=disk${i}.img
            LOOPDEV=`losetup -j ${DISK} | cut -d: -f1`
            if [ -f ${DISK} ] & [ ! -z ${LOOPDEV} ]; then
                MNTPATH=disk${i}
                if [ -d "${MNTPATH}" ]; then
                    pushd "${MNTPATH}"
                    rm -vf *
                    popd
                fi
                # umount device
                if mount | grep -q ${LOOPDEV}; then
                    echo "Umounting ${MNTPATH}..."
                    sudo umount ${MNTPATH}
                fi
                # remove directory
                if [ -d ${MNTPATH} ]; then
                    echo "Removing mount path directory..."
                    rmdir -p ${MNTPATH}
                fi
                # remove loopback device
                echo "Unbinding loopback device ${LOOPDEV} from ${DISK}..."
                losetup -d ${LOOPDEV}
            fi
        done
    fi
}

function create_snapraid_conf () {
    DISKNUM=$1
    CONF=snapraid.conf
    if [ ${DISKNUM} -gt 0 ]; then
        for i in `seq ${DISKNUM} -1 1`; do
            MNTPATH=disk${i}
            FULLPATH="`pwd`/${MNTPATH}"
            if [ $i -eq ${DISKNUM} ]; then
                echo "parity  ${FULLPATH}/parity.par" > ${CONF}
            else
                echo "content ${FULLPATH}/content" >> ${CONF}
                echo "disk d${i} ${FULLPATH}" >> ${CONF}
            fi
        done
        cat >> ${CONF} <<EOF
# Excludes any file named "*.unrecoverable"
exclude *.unrecoverable
# Excludes the root directory "/lost+found"
exclude /lost+found/
# Excludes any sub-directory named "tmp"
exclude tmp/
block_size 512
EOF
    fi
}

function fill_disk () {
    DISKNUM=$1
    if [ ${DISKNUM} -gt 0 ]; then
        for i in `seq ${DISKNUM} -1 1`; do
            MNTPATH=disk${i}
            if [ $i -ne ${DISKNUM} ]; then
                pushd ${MNTPATH}
                NUM=$((RANDOM%10 + 10))
                rm *.dat
                echo "Creating ${NUM} files..."
                for j in `seq ${NUM}`; do
                    FILE=`printf "file%03d.dat" $j`
                    SIZE=$((RANDOM%100 + 10))
                    echo "Creating file ${FILE} of size ${SIZE}kB..."
                    dd if=/dev/urandom of=${FILE} bs=1k count=${SIZE}
                done
                md5sum *.dat > ${MNTPATH}.md5
                popd
            fi
        done
    fi
}

case "$1" in
create)
        create_images 5
        ;;
remove)
        remove_images 5
        ;;
mount)
        mount_images 5
        ;;
umount)
        umount_images 5
        ;;
config)
        create_snapraid_conf 5
        ;;
filldisk)
        fill_disk 5
        ;;
*)
        echo "Usage: $0 {create|remove|mount|umount}"
        exit 1
        ;;
esac
