Introduction
------------

Simple script to test SnapRAID.

The test script can create disk image, mount them as loopback devices
and fill the disks with random data. You can then play with the
snapraid commands.

It is currently hardwired to uses five disk images. But can easily be
changed.

Example
-------

Create the disk images:

  `shell> ./test_snapraid.sh create`

Mount the disk images:

  `shell> ./test_snapraid.sh mount`

Fill the disk with random data:

  `shell> ./test_snapraid.sh filldisk`

Create an example snapraid config file using disk[1-4] as the disk
pool and disk5 as the parity disk:

  `shell> ./test_snapraid.sh config`

At this point, you can test snapraid command, i.e.:

  `shell> snapraid -c snapraid.conf sync`

  `shell> snapraid -c snapraid.conf check`

  `shell> snapraid -c snapraid.conf status`

  `shell> rm disk1/file002.dat`

  `shell> dd if=/dev/urandom of=disk2/junk.dat bs=1k count=200`

  `shell> snapraid -c snapraid.conf scrub`

  `shell> snapraid -c snapraid.conf fix`

  `shell> snapraid -c snapraid.conf diff`

  `shell> snapraid -c snapraid.conf sync`

Once you've finish, you can clean up by:

   `shell> ./test_snapraid.sh umount`

   `shell> ./test_snapraid.sh remove`
